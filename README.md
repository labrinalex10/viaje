# viaje

## Requisitos

1. Comando curl
2. Comando cat
3. Internet

## Primeros pasos

Creamos la carpeta oculta en el /home/${USER}/.viaje

$ mkdir ~/.viaje

Copiamos el archivo a $PATH (Puedes saber la ubicación con echo $PATH)

$ sudo cp viaje /usr/local/bin/

## Uso

Ejecutamos escribiendo la palabra viaje, luego escribimos el nombre del país, e inmediatamente descarga y muestra el nivel de riesgo del país.

$ viaje

## Recomendaciones

Es necesario escribir el nombre de los países en inglés para que funcione correctamente.
